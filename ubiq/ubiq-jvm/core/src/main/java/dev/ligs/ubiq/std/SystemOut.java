package dev.ligs.ubiq.std;

import dev.ligs.ubiq.interpret.Printer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class SystemOut implements Out {
    public void write(Object what) {
        String toWrite = Printer.display(what);

        try {
            System.out.write(toWrite.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
