import {StringValue, Atom} from './values.js'

export function Token(value) {
    return {
        type: 'atom',
        value: value,
    }
}

Token.OpenParen = {type: 'open-paren'};
Token.CloseParen = {type: 'close-paren'};
Token.End = {type: 'end'};


export class Tokens {
    constructor(text) {
        this.text = text;
        this.index = 0;
    }

    [Symbol.iterator] = function* () {
        var buffer = "";
        var inString = false;
        var escaping = false;

        for (; this.index < this.text.length; this.index++) {
            let c = this.text[this.index];

            if (c === '(' && !inString) {
                yield Token.OpenParen;
            } else if (c === ')' && !inString) {
                if (buffer !== "") {
                    yield new Token(Atom.parse(buffer));
                    buffer = "";
                }
                yield Token.CloseParen;
            } else if (c === "'" && !inString) {
                inString = true;
            } else if (c === "'" && inString && !escaping) {
                inString = false
                yield new Token(new StringValue(buffer));
                buffer = ""
            } else if (c === "\\" && inString && !escaping) {
                escaping = true;
            } else if (escaping && inString) {
                buffer += c;
                escaping = false;
            } else if (/\s/.test(c) && !inString) {
                yield new Token(Atom.parse(buffer));
                buffer = "";
            } else {
                buffer += c;
            }
        }

        if (buffer !== "") {
            yield new Token(Atom.parse(buffer));
        }
    }
}
