export class SymbolValue {
    constructor(name) {
        this.name = name;
    }

    eval(env) {
        return env[this.name]
    }

    display() {
        return `\`${this.name}`
    }
}

export class StringValue {
    constructor(value) {
        this.value = value;
    }

    eval() {
        return this
    }

    display() {
        return `${this.value}`
    }
}

export class NumberValue {
    constructor(value) {
        this.value = value;
    }

    eval() {
        return this
    }

    display() {
        return `${this.value}`
    }
}

export class Nil {
    eval() {
        return this
    }

    display() {
        return "nil"
    }
}

export class ListValue {
    constructor(items) {
        this.items = items;
    }

    push(elem) {
        this.items.push(elem);
    }

    eval(env) {
        let fn = this.items[0].eval(env);
        let args = this.items.slice(1)
        return fn(env, args);
    }

    display() {
        return "" + this.items
    }
}

export class Atom {
    static parse(buffer) {
        if (buffer === "") {
            return new Nil();
        } else if (/&\d/.test(buffer)) {
            try {
                return new NumberValue(parseInt(buffer));
            } catch {
                return new NumberValue(parseFloat(buffer));
            }
        } else {
            return new SymbolValue(buffer);
        }
    }
}
