import os
import subprocess
from pathlib import Path

import pytest
from mistletoe import Document
from mistletoe.ast_renderer import get_ast

from ubiq import Interpreter, DummyOut

BASE = Path(os.path.dirname(os.path.realpath(__file__))).parent


def pytest_collect_file(path, parent):
    if path.ext == ".md":
        return MarkdownTestFile.from_parent(parent, fspath=path)


class MarkdownTestFile(pytest.File):
    last_heading = None
    code_fence = None
    runner = None

    def collect(self):
        with open(self.fspath) as f:
            ast = get_ast(Document(f))
        for runner in stdinrunners.items():
            self.runner = runner
            yield from self.visit(ast)

    def quote_text(self, quote):
        output = ""
        for child in quote["children"]:
            output += ''.join(c["content"] for c in child['children']) + "\n"
        return output

    def markdown_test(self, heading, fence, quote):
        print(f'fence {fence}')
        return MarkdownItem.from_parent(self,
                                        name=f"{heading['children'][0]['content']}[{self.runner[0]}]",
                                        runner=self.runner[1],
                                        code=''.join([c["content"] for c in fence["children"]]),
                                        expected_output=self.quote_text(quote)
                                        )

    def visit(self, node):
        print(f'visiting {node}')
        global last_heading
        global code_fence

        if node["type"] == "Heading":
            last_heading = node
        elif node["type"] == "CodeFence":
            code_fence = node
        elif node["type"] == "Quote":
            yield self.markdown_test(last_heading, code_fence, node)

        if "children" in node:
            for child in node["children"]:
                yield from self.visit(child)


class MarkdownItem(pytest.Item):
    def __init__(self, name, parent, runner, code, expected_output):
        super().__init__(name, parent)
        self.runner = runner
        self.code = code
        self.expected_output = expected_output

    def runtest(self):
        output = self.runner(self.code)
        assert output == self.expected_output

    def repr_failure(self, excinfo):
        return str(excinfo)

    def reportinfo(self):
        return self.fspath, 0, f"usecase: {self.name}"


def swift_run(inputfile):
    path = BASE / 'UbiqApple'
    print('running swift')
    return subprocess.check_output(
        ['swift', 'run', 'ubiqrunner', inputfile],
        cwd=path
    ).decode('utf-8')


def gradle_run(inputfile):
    path = BASE / 'ubiq-jvm'
    print('running gradle')
    return subprocess.check_output(
        ['./gradlew', 'run', '-q', f'--args="{inputfile}"'],
        cwd=path
    ).decode('utf-8')


def npm_run(inputfile):
    path = BASE / 'ubiq-js'
    return subprocess.check_output(
        f"zsh --login -c 'nvm use --lts > /dev/null && npm run -s start {inputfile}'",
        cwd=path,
        env={'NODE_NO_WARNINGS': '1'},
        shell=True
    ).decode('utf-8')


def python_run(inputfile):
    interpreter = Interpreter.merging_env({'out': DummyOut()})
    interpreter.interpret(inputfile)
    return interpreter['out'].output


runners = {
    "swift": swift_run,
    "gradle": gradle_run,
    "npm": npm_run,
    "python": python_run,
}


@pytest.fixture(params=runners.keys())
def runner(request):
    return runners[request.param]


def swift_run_stdin(inputtext):
    path = BASE / 'UbiqApple'
    print('running swift')
    return subprocess.check_output(
        ['swift', 'run', 'ubiqrunner', '-i'],
        cwd=path,
        input=inputtext.encode('utf-8')
    ).decode('utf-8')


def gradle_run_stdin(inputtext):
    path = BASE / 'ubiq-jvm'
    print('running gradle')
    subprocess.check_call(
        [
            './gradlew', 'clean', 'assemble', '--console=plain', '-q'
        ],
        cwd=path,
    )
    return subprocess.check_output(
        [
            'java', '-jar', './core/build/libs/core-1.0-SNAPSHOT.jar', '-i'
        ],
        cwd=path,
        input=inputtext.encode('utf-8')
    ).decode('utf-8')


def npm_run_stdin(inputtext):
    path = BASE / 'ubiq-js'
    return subprocess.check_output(
        f"zsh --login -c 'nvm use --lts > /dev/null && npm run -s start -- -i'",
        cwd=path,
        env={'NODE_NO_WARNINGS': '1'},
        shell=True,
        input=inputtext.encode('utf-8')
    ).decode('utf-8')


def python_run_stdin(inputtext):
    interpreter = Interpreter.merging_env({'out': DummyOut()})
    interpreter.interpret_text(inputtext)
    return interpreter['out'].output


stdinrunners = {
    "swift": swift_run_stdin,
    "gradle": gradle_run_stdin,
    "npm": npm_run_stdin,
    "python": python_run_stdin,
}
