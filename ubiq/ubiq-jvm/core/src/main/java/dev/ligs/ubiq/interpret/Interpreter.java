package dev.ligs.ubiq.interpret;

import dev.ligs.ubiq.std.Displayln;
import dev.ligs.ubiq.std.Out;
import dev.ligs.ubiq.std.Quit;
import dev.ligs.ubiq.std.SystemOut;
import dev.ligs.ubiq.sys.Environment;
import dev.ligs.ubiq.sys.exceptions.UbiqException;
import dev.ligs.ubiq.sys.exceptions.UbiqRuntimeException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Interpreter {
    public static Environment prologue(Out out) {
        Environment prologue = new Environment();
        prologue.put("out", out);
        prologue.put("displayln", new Displayln());
        prologue.put("quit", new Quit());
        return prologue;
    }

    private final Environment environment;
    private final Out out;

    public Interpreter() {
        this.out = new SystemOut();
        this.environment = prologue(out);
    }

    public Interpreter(Environment environment) {
        this.environment = environment;
        this.out = (Out) environment.get("out");
    }

    public void interpret(Path inputFile) throws UbiqException {
        try {
            String script = Files.readString(inputFile);
            interpret(script);
        } catch (IOException e) {
            throw new UbiqRuntimeException(e);
        }
    }

    public void interpret(String inputScript) throws UbiqException {
        interpret(new Reader(inputScript));
    }

    public void interpret(Reader reader) throws UbiqException {
        while (reader.hasNext()) {
            Object form = reader.next();
            this.interpretForm(form);
        }
    }

    public void interpretForm(Object value) {
        Object last = Evaluator.eval(value, this.environment);
        this.environment.put("*", last);
    }

    public Object getLast() {
        return this.environment.get("*");
    }

    public boolean hasQuit() {
        return this.environment.hasQuit();
    }
}
