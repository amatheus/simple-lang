package dev.ligs.ubiq.interpret;

import dev.ligs.ubiq.sys.Environment;

import java.util.List;
import java.util.function.BiFunction;

public class Evaluator {
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static Object eval(Object form, Environment env) {
        if (form instanceof List) {
            List l = (List) form;
            BiFunction<Environment, List, Object> fn =
                    (BiFunction<Environment, List, Object>) eval(l.get(0), env);
            List args = l.subList(1, l.size());
            return fn.apply(env, args);
        } else if (form instanceof Evaluatable) {
            Evaluatable e = (Evaluatable) form;
            return e.eval(env);
        } else {
            return form;
        }
    }
}
