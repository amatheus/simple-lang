package dev.ligs.ubiq.std;

import dev.ligs.ubiq.interpret.Printer;
import dev.ligs.ubiq.sys.Environment;

import java.util.List;
import java.util.function.BiFunction;

import static dev.ligs.ubiq.value.Nil.nil;

public class Displayln implements BiFunction<Environment, List<Object>, Object> {
    @Override
    public Object apply(Environment environment, List<Object> args) {
        Out out = (Out) environment.get("out");
        assert args.size() == 1;
        Object what = args.get(0);
        out.write(Printer.display(what));
        out.write("\n");
        return nil;
    }
}
