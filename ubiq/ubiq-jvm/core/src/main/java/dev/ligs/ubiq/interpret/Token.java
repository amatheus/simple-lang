package dev.ligs.ubiq.interpret;

import static dev.ligs.ubiq.value.Nil.nil;

public class Token {
    public final Object value;
    public final Type type;

    public Token(Object value, Type type) {
        this.value = value;
        this.type = type;
    }

    public Token(Type type) {
        this.value = nil;
        this.type = type;
    }

    public enum Type {
        OPEN_PAREN,
        CLOSE_PAREN,
        ATOM,
        END,
    }

    @Override
    public String toString() {
        return "Token{" +
                "value=" + value +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Token && this.type.equals(((Token) obj).type) &&
                this.value.equals(((Token) obj).value);
    }
}
