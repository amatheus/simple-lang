import assert from 'assert';
import {Reader} from "../src/reader.js";
import {ListValue, StringValue, SymbolValue} from "../src/values.js";

describe('Reader', () => {
    it('should read hello world', () => {
        let reader = new Reader("(displayln 'a')");
        let actual = [...reader];
        let expected = [
            new ListValue([
                new SymbolValue("displayln"),
                new StringValue("a"),
            ])
        ];

        assert.deepStrictEqual(actual, expected);
    });
});
