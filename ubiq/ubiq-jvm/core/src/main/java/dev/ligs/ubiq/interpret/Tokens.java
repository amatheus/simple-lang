package dev.ligs.ubiq.interpret;

import dev.ligs.ubiq.value.Atom;

import java.util.Iterator;

public class Tokens implements Iterator<Token> {
    private String text;
    private int index = 0;

    public Tokens(String text) {
        this.text = text;
    }

    @Override
    public boolean hasNext() {
        return index < text.length();
    }

    @Override
    public Token next() {
        StringBuilder buffer = new StringBuilder();
        boolean inString = false;
        boolean escaping = false;

        while (hasNext()) {
            char c = text.charAt(index);

            if (c == '(' && !inString) {
                index++;
                return new Token(Token.Type.OPEN_PAREN);
            } else if (c == ')' && !inString) {
                if (buffer.length() > 0) {
                    return new Token(Atom.parse(buffer.toString()), Token.Type.ATOM);
                } else {
                    index++;
                    return new Token(Token.Type.CLOSE_PAREN);
                }
            } else if (c == '\'' && !inString) {
                inString = true;
                index++;
            } else if (c == '\'' && !escaping) {
                index++;
                return new Token(buffer.toString(), Token.Type.ATOM);
            } else if (c == '\\' && inString) {
                escaping = true;
                index++;
            } else if (escaping) {
                buffer.append(c);
                escaping = false;
                index++;
            } else if (Character.isSpaceChar(c) && !inString) {
                if (buffer.length() > 0) {
                    index++;
                    return new Token(Atom.parse(buffer.toString()), Token.Type.ATOM);
                }
            } else {
                buffer.append(c);
                index++;
            }
        }

        if (buffer.length() > 0) {
            return new Token(Atom.parse(buffer.toString()), Token.Type.ATOM);
        } else {
            return new Token(Token.Type.END);
        }
    }
}
