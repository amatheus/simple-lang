import assert from 'assert';
import {Interpreter} from "../src/interpreter.js";

class DummyOut {
    output = "";

    write(what) {
        if (typeof what === 'string') {
            this.output += what
        } else {
            this.output += what.display()
        }
    }
}

describe('Interpreter', () => {
    it('should interpret hello world', () => {
        let dummyOut = new DummyOut();
        let environment = Interpreter.prologue(dummyOut);
        let interpreter = new Interpreter(environment);
        interpreter.interpret("(displayln 'Hello world!')");
        assert.equal("Hello world!\n", dummyOut.output);
    });
});
