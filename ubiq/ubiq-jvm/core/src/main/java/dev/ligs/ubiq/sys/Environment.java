package dev.ligs.ubiq.sys;

import java.util.HashMap;
import java.util.Map;

public class Environment {
    private final Map<String, Object> environment = new HashMap<>();
    private boolean shouldQuit = false;

    public void quit() {
        shouldQuit = true;
    }

    public void put(String name, Object value) {
        this.environment.put(name, value);
    }

    public Object get(String name) {
        return this.environment.get(name);
    }

    public boolean hasQuit() {
        return shouldQuit;
    }
}
