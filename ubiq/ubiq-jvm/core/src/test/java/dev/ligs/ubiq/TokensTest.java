package dev.ligs.ubiq;

import dev.ligs.ubiq.interpret.Token;
import dev.ligs.ubiq.interpret.Tokens;
import dev.ligs.ubiq.value.Symbol;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class TokensTest {
    @Test
    void shouldTokenizeHelloWorld() {
        List<Token> tokens = new ArrayList<>();
        new Tokens("(displayln 'a')")
                .forEachRemaining(tokens::add);
        List<Token> expected = Arrays.asList(
                new Token(Token.Type.OPEN_PAREN),
                new Token(new Symbol("displayln"), Token.Type.ATOM),
                new Token("a", Token.Type.ATOM),
                new Token(Token.Type.CLOSE_PAREN)
        );

        Assertions.assertEquals(expected, tokens);
    }
}