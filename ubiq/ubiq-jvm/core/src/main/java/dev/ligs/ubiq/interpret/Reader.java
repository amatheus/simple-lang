package dev.ligs.ubiq.interpret;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import static dev.ligs.ubiq.value.Nil.nil;

public class Reader implements Iterator<Object> {
    private final Tokens tokens;

    public Reader(String text) {
        this.tokens = new Tokens(text);
    }

    @Override
    public boolean hasNext() {
        return this.tokens.hasNext();
    }

    @Override
    public Object next() {
        Stack<List<Object>> valueStack = new Stack<>();

        while (hasNext()) {
            Token token = this.tokens.next();
            if (token.type.equals(Token.Type.OPEN_PAREN)) {
                valueStack.push(new ArrayList<>());
            } else if (token.type.equals(Token.Type.CLOSE_PAREN)) {
                Object value = valueStack.pop();
                if (valueStack.empty()) {
                    return value;
                } else {
                    valueStack.peek().add(value);
                }
            } else if (!valueStack.isEmpty()) {
                valueStack.peek().add(token.value);
            } else {
                return token.value;
            }
        }

        if (!valueStack.isEmpty()) {
            return valueStack.pop();
        } else {
            return nil;
        }
    }
}
