import {SymbolValue, StringValue, NumberValue, Nil, Atom} from "./src/values.js";
import {Token, Tokens} from './src/tokens.js'
import fs from 'fs';
import {Interpreter} from "./src/interpreter.js";

export {
    SymbolValue, StringValue, NumberValue, Nil, Atom,
    Token, Tokens
}


if (process.argv[2] === "-i") {
    var script = "";
    process.stdin.on('data', function (data) {
        script += data;
    }).on('end', function () {
        let interpreter = new Interpreter();
        interpreter.interpret(script);
    });
} else {
    fs.readFile(process.argv[2], "utf8", (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let interpreter = new Interpreter();
            interpreter.interpret(data);
        }
    });
}
