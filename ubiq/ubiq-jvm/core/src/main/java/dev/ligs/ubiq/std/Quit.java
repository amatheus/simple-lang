package dev.ligs.ubiq.std;

import dev.ligs.ubiq.sys.Environment;

import java.util.List;
import java.util.function.BiFunction;

import static dev.ligs.ubiq.value.Nil.nil;

public class Quit implements BiFunction<Environment, List<Object>, Object> {
    @Override
    public Object apply(Environment environment, List<Object> args) {
        environment.quit();
        return nil;
    }
}
