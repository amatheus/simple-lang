import {Reader} from "./reader.js";
import {displayln, SystemOut} from "./std.js";

class Environment {
}

export class Interpreter {
    static prologue(out) {
        if (out === undefined) {
            out = new SystemOut();
        }
        let prologue = new Environment();
        prologue["out"] = out;
        prologue["displayln"] = displayln;
        return prologue;
    }

    constructor(environment) {
        if (environment === undefined) {
            this.environment = Interpreter.prologue()
        } else {
            this.environment = environment;
        }
    }


    interpret(script) {
        let reader = new Reader(script)[Symbol.iterator]();
        for (let form of reader) {
            this.environment["*"] = form.eval(this.environment);
        }
    }
}
