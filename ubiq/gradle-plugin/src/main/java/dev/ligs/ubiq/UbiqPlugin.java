package dev.ligs.ubiq;

import dev.ligs.ubiq.interpret.Interpreter;
import dev.ligs.ubiq.sys.exceptions.UbiqException;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.nio.file.Path;

public class UbiqPlugin implements Plugin<Project> {
    @Override
    public void apply(Project project) {
        UbiqPluginExtension extension = project.getExtensions()
                .create("ubiq", UbiqPluginExtension.class);

        project.task("compileSimple")
                .doLast(task -> {
                    if (extension.getScriptfile() != null) {
                        Interpreter interpreter = new Interpreter();
                        try {
                            System.out.println("interpreting " + extension.getScriptfile().getAbsolutePath());
                            interpreter.interpret(Path.of(extension.getScriptfile().getAbsolutePath()));
                            System.out.println(interpreter.getLast());
                        } catch (UbiqException e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println("setup ubiq first!");
                    }
                });

        if (extension.getScriptfile() == null) {
            System.out.println("setup ubiq first!");
        }
    }
}
