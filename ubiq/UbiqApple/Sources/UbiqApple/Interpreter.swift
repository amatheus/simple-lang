import Foundation

enum Token : Equatable {
    case OpenParen
    case CloseParen
    case Atom(Atom)
    case End
    case SyntaxError
}

struct Tokens: IteratorProtocol {
    typealias Element = Token

    private var chars: String.Iterator
    private var index = 0
    private var nextToken: Token? = nil

    init(text: String) {
        chars = text.makeIterator()
    }

    mutating func next() -> Token? {
        if let token = nextToken {
            nextToken = nil
            return token
        }
        var buffer = ""
        var inString = false
        var escaping = false

        while let c = chars.next() {
            if c == "(" && !inString {
                return Token.OpenParen
            } else if c == ")" && !inString {
                if buffer.count > 0 {
                    nextToken = Token.CloseParen
                    do {
                        return Token.Atom(try parseAtom(input: buffer))
                    } catch {
                        return Token.SyntaxError
                    }
                } else {
                    return Token.CloseParen
                }
            } else if c == "\'" && !inString {
                inString = true
            } else if c == "\'" && !escaping {
                return Token.Atom(Atom.String(value: buffer))
            } else if c == "\\" && inString {
                escaping = true
            } else if (escaping) {
                buffer.append(c)
                escaping = false
            } else if c.isWhitespace && !inString && buffer.count > 0 {
                do {
                    return Token.Atom(try parseAtom(input: buffer))
                } catch {
                    return Token.SyntaxError
                }
            } else {
                buffer.append(c)
            }
        }

        if buffer.count > 0 {
            do {
                return Token.Atom(try parseAtom(input: buffer))
            } catch {
                return Token.SyntaxError
            }
        } else {
            return nil
        }
    }
}

struct Reader: IteratorProtocol {
    enum Result {
        case Ok(Value)
        case Error
    }

    typealias Element = Result

    var tokens: Tokens

    mutating func next() -> Result? {
        var values: [ListValue] = []

        while let token = tokens.next() {
            if case .OpenParen = token {
                values.append(ListValue())
            } else if case .CloseParen = token {
                let value = values.popLast()

                if let value = value {
                    if let last = values.last {
                        last.append(value: Value.List(value))
                    } else {
                        return .Ok(Value.List(value))
                    }
                } else {
                    return .Error
                }
            } else if let last = values.last, case let .Atom(atom) = token {
                last.append(value: Value.Atom(atom))
            } else if case let .Atom(atom) = token {
                return .Ok(Value.Atom(atom))
            }
        }

        if let last = values.popLast() {
            return .Ok(Value.List(last))
        } else {
            return nil
        }
    }
}

public class Interpreter {
    static func prologue() -> Environment {
        let values: [String: Evaluatable] = [
            "out": SystemOut(),
            "displayln": Displayln(),
        ]
        return Environment(values: values)
    }

    var environment: Environment

    public init(environment: Environment) {
        self.environment = environment
    }

    public convenience init() {
        self.init(environment: Interpreter.prologue())
    }

    public func interpretFile(inputScriptPath: String) throws {
        let script = try String(contentsOfFile: inputScriptPath)
        try interpretScript(script: script)
    }

    public func interpretScript(script: String) throws {
        var reader = Reader(tokens: Tokens(text: script))
        while case let .Ok(value) = reader.next() {
            try interpret(value: value)
        }
    }

    func interpret(value: Evaluatable) throws {
        let last = try value.eval(environment: environment)
        environment.put("*", last)
    }
}