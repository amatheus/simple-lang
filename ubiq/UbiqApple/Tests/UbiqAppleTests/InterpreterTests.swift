import XCTest
@testable import UbiqApple

final class InterpreterTests: XCTestCase {
    class DummyOut: Out, Evaluatable {
        func eval(environment: Environment) throws -> Evaluatable {
            self
        }

        func print() -> String {
            "<DummyOut>"
        }

        func display() -> String {
            print()
        }

        var output: String = ""

        func write(what: Evaluatable) {
            output += what.display()
        }

        func write(what: String) {
            output += what
        }
    }

    func testShouldInterpretHelloWorld() throws {
        let env = Interpreter.prologue()
        let dummyOut = DummyOut()
        env.put("out", dummyOut)
        let interpreter = Interpreter(environment: env)
        try interpreter.interpretScript(script: "(displayln 'Hello world!')")

        XCTAssertEqual(dummyOut.output, "Hello world!\n")
    }
}
