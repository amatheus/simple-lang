package dev.ligs.ubiq.sys.exceptions;

public class UbiqRuntimeException extends UbiqException {
    public UbiqRuntimeException(Throwable cause) {
        super("runtime-exception", cause);
    }
}
