import {Token, Tokens} from "./tokens.js";
import {ListValue} from "./values.js";

export class Reader {
    constructor(text) {
        this.tokens = new Tokens(text)[Symbol.iterator]();
    }

    [Symbol.iterator] = function* () {
        let values = [];

        let result = this.tokens.next();
        while (!result.done) {
            let token = result.value;

            if (token === Token.OpenParen) {
                values.push(new ListValue([]));
            } else if (token === Token.CloseParen) {
                let value = values.pop();
                if (values.length === 0) {
                    yield value;
                } else {
                    values[values.length - 1].push(value);
                }
            } else if (values.length !== 0) {
                values[values.length - 1].push(token.value);
            } else {
                yield token.value;
            }

            result = this.tokens.next();
        }
    }
}
