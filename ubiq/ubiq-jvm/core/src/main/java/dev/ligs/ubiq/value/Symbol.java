package dev.ligs.ubiq.value;

import dev.ligs.ubiq.interpret.Evaluatable;
import dev.ligs.ubiq.sys.Environment;

public class Symbol implements Evaluatable {
    private final String name;

    public Symbol(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Symbol[" + name + "]";
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Symbol && this.name.equals(((Symbol) obj).name);
    }

    @Override
    public Object eval(Environment environment) {
        return environment.get(name);
    }

    public String print() {
        return ":"  + this.name;
    }

    public String display() {
        return this.print();
    }
}
