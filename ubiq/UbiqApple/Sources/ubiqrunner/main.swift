import Foundation
import UbiqApple

if CommandLine.arguments.count == 2 {
    if CommandLine.arguments[1] == "-i" {
        var s = ""
        while let readString = readLine() {
            s += readString
        }
        let interpreter = Interpreter()
        try interpreter.interpretScript(script: s)
    } else {
        let interpreter = Interpreter()
        try interpreter.interpretFile(inputScriptPath: CommandLine.arguments[1])
    }
}
