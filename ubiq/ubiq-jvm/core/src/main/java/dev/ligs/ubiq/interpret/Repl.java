package dev.ligs.ubiq.interpret;

import dev.ligs.ubiq.sys.exceptions.UbiqException;

import java.util.Scanner;

public class Repl {
    public void replLoop() {
        Interpreter interpreter = new Interpreter();
        Scanner scanner = new Scanner(System.in);

        System.out.print("> ");
        System.out.flush();
        while (scanner.hasNextLine() && !interpreter.hasQuit()) {
            String nextLine = scanner.nextLine();
            Reader reader = new Reader(nextLine);
            try {
                interpreter.interpret(reader);
                System.out.println(": " + Printer.print(interpreter.getLast()));
            } catch (UbiqException e) {
                System.out.println("! ERROR:");
                System.out.println("! " + e.getMessage());
            }
            System.out.print("> ");
            System.out.flush();
        }
    }
}
