import sys
from collections import UserString
from dataclasses import dataclass


def displayln(env, what):
    env['out'].write(what)
    env['out'].write('\n')


class Symbol(UserString):
    def eval(self, env):
        return env[self]


class Out:
    def write(self, what):
        sys.stdout.write(what)


@dataclass
class DummyOut:
    output = ''

    def write(self, what):
        self.output += what


class _Nil:
    pass


Nil = _Nil()


class Interpreter:
    default_environment = {
        'out': Out(),
        'displayln': displayln,
    }

    def __init__(self, env=None):
        if env is None:
            self.env = Interpreter.default_environment.copy()
        else:
            self.env = env

    @classmethod
    def merging_env(cls, env):
        new_env = Interpreter.default_environment.copy()
        for k, v in env.items():
            new_env[k] = v
        return Interpreter(new_env)

    def interpret(self, inputfile):
        with open(inputfile) as f:
            inputtext = f.read()
        self.interpret_text(inputtext)

    def interpret_text(self, inputtext):
        for form in read(inputtext):
            eval(form, self.env)

    def __getitem__(self, item):
        return self.env[item]


def eval(form, env):
    if isinstance(form, list):
        f = eval(env[form[0]], (env))
        args = [eval(arg, env) for arg in form[1:]]
        return f(env, *args)
    elif hasattr(form, 'eval'):
        return form.eval(env)
    else:
        return form


def read(text):
    stack = []
    for token in tokens(text):
        if token == '(':
            stack.append([])
        elif token == ')':
            el = stack.pop()
            if stack:
                stack[-1].append(el)
            else:
                yield el
        elif stack:
            stack[-1].append(token)
        else:
            stack.append(token)
    while stack:
        yield stack.pop()


def tokens(text):
    buffer = ''
    in_string = False
    escaping = False
    for c in text:
        if c == '(' and not in_string:
            yield c
        elif c == ')' and not in_string:
            if buffer:
                yield buffer
                buffer = ''
            yield c
        elif c == "'" and not in_string:
            in_string = True
        elif c == "'" and in_string:
            in_string = False
            if buffer:
                yield buffer
                buffer = ''
        elif c == "\\" and in_string:
            escaping = True
        elif escaping and in_string:
            buffer += c
            escaping = False
        elif c.isspace() and not in_string:
            if buffer:
                yield parse_atom(buffer)
                buffer = ''
            continue
        else:
            buffer += c
    if buffer:
        yield buffer


def parse_atom(buffer):
    if buffer == '':
        return Nil
    elif buffer[0].isdigit():
        try:
            return float(buffer)
        except ValueError:
            return int(buffer)
    else:
        return Symbol(buffer)
