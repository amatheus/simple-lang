import XCTest
@testable import UbiqApple

final class ReaderTests: XCTestCase {
    func testShouldReadHelloWorld() throws {
        var values: [Value] = []
        var iter = Reader(tokens: Tokens(text: "(displayln 'a')"))
        while case let .Ok(value) = iter.next() {
            values.append(value)
        }

        let expected: [Value] = [
            Value.List(ListValue(values: [
                Value.Atom(Atom.Symbol(name: "displayln")),
                Value.Atom(Atom.String(value: "a"))
            ]))
        ]

        XCTAssertEqual(values, expected)
    }
}
