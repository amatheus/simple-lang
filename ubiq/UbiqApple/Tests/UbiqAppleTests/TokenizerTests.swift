import XCTest
@testable import UbiqApple

final class TokenizerTests: XCTestCase {
    func testShouldTokenizeHelloWorld() throws {
        var tokens: [Token] = []
        var iter = Tokens(text: "(displayln 'a')")
        while let token = iter.next() {
            tokens.append(token)
        }
        let expected = [
            Token.OpenParen,
            Token.Atom(Atom.Symbol(name: "displayln")),
            Token.Atom(Atom.String(value: "a")),
            Token.CloseParen
        ]

        XCTAssertEqual(tokens, expected)
    }
}
