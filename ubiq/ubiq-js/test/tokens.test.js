import {Tokens, Token, SymbolValue, StringValue} from '../index.js';

import assert from 'assert';

describe('Tokens', () => {
    it('should tokenizer hello world', () => {
        let tokens = new Tokens("(displayln 'a')");
        let actual = [...tokens];
        let expected = [
            Token.OpenParen,
            Token(new SymbolValue("displayln")),
            Token(new StringValue("a")),
            Token.CloseParen,
        ];

        assert.deepStrictEqual(actual, expected);
    })
});
