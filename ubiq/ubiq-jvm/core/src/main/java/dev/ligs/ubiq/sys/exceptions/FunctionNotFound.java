package dev.ligs.ubiq.sys.exceptions;

import dev.ligs.ubiq.interpret.Printer;

public class FunctionNotFound extends UbiqException {
    public FunctionNotFound(String functionName) {
        super("undefined-function", "Function " + Printer.print(functionName) + " not found");
    }
}
