package dev.ligs.ubiq;

import dev.ligs.ubiq.interpret.Interpreter;
import dev.ligs.ubiq.interpret.Printer;
import dev.ligs.ubiq.std.Out;
import dev.ligs.ubiq.sys.Environment;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InterpreterTest {
    class DummyOut implements Out {
        private StringBuilder output = new StringBuilder();

        @Override
        public void write(Object what) {
            output.append(Printer.display(what));
        }

        public String getOutput() {
            return output.toString();
        }
    }

    @Test
    void shouldInterpretHelloWorld() throws Exception {
        DummyOut dummyOut = new DummyOut();
        Environment environment = Interpreter.prologue(dummyOut);
        Interpreter interpreter = new Interpreter(environment);
        interpreter.interpret("(displayln 'Hello world!')");
        assertEquals("Hello world!\n", dummyOut.getOutput());
    }
}