package dev.ligs.ubiq.sys.exceptions;

public class UbiqException extends Exception {
    protected final String name;

    public UbiqException(String name, String message) {
        super(message);
        this.name = name;
    }

    public UbiqException(String name, String message, Throwable cause) {
        super(message, cause);
        this.name = name;
    }

    public UbiqException(String name, Throwable cause) {
        super(cause);
        this.name = name;
    }

    @Override
    public String getMessage() {
        return "[" + this.name + "] " + super.getMessage();
    }
}
