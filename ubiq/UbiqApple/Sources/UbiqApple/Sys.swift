import Foundation

public class Environment {
    var values: [String: Evaluatable]

    init() {
        values = [:]
    }

    init(values: [String: Evaluatable]) {
        self.values = values
    }

    func get(_ name: String) -> Evaluatable? {
        values[name]
    }

    func put(_ name: String, _ value: Evaluatable) {
        values[name] = value
    }
}

protocol Out {
    func write(what: Evaluatable)
    func write(what: String)
}

class SystemOut: Evaluatable, Out {
    func write(what: Evaluatable) {
        Swift.print(what.display(), terminator: "")
    }

    func write(what: String) {
        Swift.print(what, terminator: "")
    }

    func eval(environment: Environment) throws -> Evaluatable {
        self
    }

    func print() -> String {
        "<Out>"
    }

    func display() -> String {
        print()
    }
}

struct Displayln: FunctionValue, Evaluatable {
    var name = "displayln"

    func apply(environment: Environment, args: [Evaluatable]) -> Evaluatable {
        if let out = environment.get("out") as? Out {
            let what = args[0]
            out.write(what: what)
            out.write(what: "\n")
            return Value.Atom(Atom.Nil)
        }
        return Value.Atom(Atom.Nil)
    }
}