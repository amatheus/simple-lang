import Foundation

protocol Evaluatable {
    func eval(environment: Environment) throws -> Evaluatable
    func print() -> String
    func display() -> String
}

enum Value: Evaluatable, Equatable {
    static func ==(lhs: Value, rhs: Value) -> Bool {
        switch (lhs, rhs) {
        case (.Atom(let lhs), .Atom(let rhs)):
            return lhs == rhs
        case (.List(let lhs), .List(let rhs)):
            return lhs == rhs
        default:
            return false
        }
    }

    case Atom(Atom)
    case List(ListValue)
    case Function(FunctionValue & Evaluatable)

    func eval(environment: Environment) throws -> Evaluatable {
        switch self {
        case .Atom(let atom):
            return try atom.eval(environment: environment)
        case .List(let list):
            return try list.eval(environment: environment)
        case .Function(let fn):
            return fn
        }
    }

    func print() -> String {
        switch self {
        case .Atom(let atom):
            return atom.print()
        case .List(let list):
            return list.print()
        case .Function(let fn):
            return fn.print()
        }
    }

    func display() -> String {
        switch self {
        case .Atom(let atom):
            return atom.display()
        case .List(let list):
            return list.display()
        case .Function(let fn):
            return fn.display()
        }
    }
}

enum Atom: Evaluatable, Equatable {
    case Nil
    case Symbol(name: String)
    case String(value: String)
    case Number(value: NSNumber)

    func eval(environment: Environment) throws -> Evaluatable {
        switch self {
        case .Symbol(let name):
            return environment.get(name) ?? Value.Atom(Atom.Nil)
        default:
            return self
        }
    }

    func print() -> String {
        switch self {
        case .Nil:
            return "nil"
        case .Symbol(let name):
            return ":\(name)"
        case .String(let value):
            return "'\(value)'"
        case .Number(let value):
            return value.description
        }
    }

    func display() -> String {
        switch self {
        case .Nil:
            return "nil"
        case .Symbol(let name):
            return ":\(name)"
        case .String(let value):
            return "\(value)"
        case .Number(let value):
            return value.description
        }
    }
}

protocol FunctionValue {
    var name: String { get }
    func apply(environment: Environment, args: [Evaluatable]) -> Evaluatable
}

extension Equatable where Self: FunctionValue {
    internal static func ==(lhs: Self, rhs: Self) -> Bool {
        false
    }
}

extension Evaluatable where Self: FunctionValue {
    func eval(environment: Environment) throws -> Evaluatable {
        self
    }

    func print() -> String {
        "Fn<\(self.name)>"
    }

    func display() -> String {
        print()
    }
}

enum UbiqError: Error {
    case FunctionNotFound(name: String)
    case RuntimeException
    case InvalidNumberFormat
}

class ListValue: Equatable, Evaluatable {
    static func ==(lhs: ListValue, rhs: ListValue) -> Bool {
        lhs.values == rhs.values
    }

    var values: [Value]

    init(values: [Value]) {
        self.values = values
    }

    convenience init() {
        self.init(values: [])
    }

    func eval(environment: Environment) throws -> Evaluatable {
        guard let fn: FunctionValue = try values[0].eval(environment: environment) as? FunctionValue else {
            throw UbiqError.FunctionNotFound(name: values[0].print())
        }
        let args: [Evaluatable] = Array(values.suffix(from: 1))
        return fn.apply(environment: environment, args: args)
    }

    func print() -> String {
        "[\(values.map {$0.print() })]"
    }

    func display() -> String {
        "[\(values.map {$0.display() })]"
    }

    func append(value: Value) {
        values.append(value)
    }
}

func parseAtom(input: String) throws -> Atom {
    if input.isEmpty {
        return .Nil
    } else if input.first?.isNumber == true {
        if let n = Int(input) {
            return .Number(value: NSNumber.init(integerLiteral: n))
        } else if let d = Double(input) {
            return .Number(value: NSNumber.init(floatLiteral: d))
        } else {
            throw UbiqError.InvalidNumberFormat
        }
    } else {
        return .Symbol(name: input)
    }
}
