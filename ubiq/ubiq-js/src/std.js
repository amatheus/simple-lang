import {Nil} from "./values.js";

export function displayln(env, args) {
    let out = env['out'];
    let what = args[0];
    out.write(what.display());
    out.write('\n')
    return new Nil();
}

export class SystemOut {
    write(what) {
        if (typeof what === 'string') {
            process.stdout.write(what);
        } else {
            process.stdout.write(what.display());
        }
    }
}
