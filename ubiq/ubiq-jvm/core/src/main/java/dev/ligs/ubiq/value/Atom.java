package dev.ligs.ubiq.value;

import static dev.ligs.ubiq.value.Nil.nil;

public abstract class Atom {
    public static Object parse(String input) {
        if (input.isEmpty()) {
            return nil;
        } else if (Character.isDigit(input.charAt(0))) {
            try {
                return Integer.parseInt(input);
            } catch (NumberFormatException e) {
                return Double.parseDouble(input);
            }
        } else {
            return new Symbol(input);
        }
    }
}
