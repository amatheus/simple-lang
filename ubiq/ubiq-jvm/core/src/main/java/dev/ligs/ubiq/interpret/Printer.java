package dev.ligs.ubiq.interpret;

import java.util.List;
import java.util.stream.Collectors;

public class Printer {
    public static String print(Object object) {
        if (object instanceof List) {
            @SuppressWarnings("unchecked")
            List<Object> values = (List<Object>)object;
            List<String> printedValues = values.stream()
                    .map(Printer::print)
                    .collect(Collectors.toList());

            return "[" + String.join(" ", printedValues) + "]";
        } else if (object instanceof String){
            return "'" + object + "'";
        } else {
            return object.toString();
        }
    }

    public static String display(Object object) {
        if (object instanceof List) {
            @SuppressWarnings("unchecked")
            List<Object> values = (List<Object>)object;
            List<String> printedValues = values.stream()
                    .map(Printer::display)
                    .collect(Collectors.toList());

            return "[" + String.join(",", printedValues) + "]";
        } else {
            return object.toString();
        }
    }
}
