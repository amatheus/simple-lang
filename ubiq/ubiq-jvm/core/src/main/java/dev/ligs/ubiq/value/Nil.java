package dev.ligs.ubiq.value;

public class Nil {
    @SuppressWarnings("InstantiationOfUtilityClass")
    public static final Nil nil = new Nil();

    private Nil() {}
}