package dev.ligs.ubiq;

import org.gradle.api.provider.Property;

import java.io.File;

public class UbiqPluginExtension {
    private File scriptfile;

    public File getScriptfile() {
        return scriptfile;
    }

    public void setScriptfile(File scriptfile) {
        this.scriptfile = scriptfile;
    }
}
