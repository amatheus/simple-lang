package dev.ligs.ubiq;

import dev.ligs.ubiq.interpret.Reader;
import dev.ligs.ubiq.value.Symbol;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class ReaderTest {
    @Test
    void shouldReadHelloWorld() {
        List<Object> actual = new ArrayList<>();
        new Reader("(displayln 'a')").forEachRemaining(actual::add);
        List<Object> expected = List.of(
                Arrays.asList(
                        new Symbol("displayln"),
                        "a"
                )
        );
        Assertions.assertEquals(expected, actual);
    }
}