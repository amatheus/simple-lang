package dev.ligs.ubiq;

import dev.ligs.ubiq.interpret.Interpreter;
import dev.ligs.ubiq.interpret.Repl;
import dev.ligs.ubiq.sys.exceptions.UbiqException;

import java.io.IOException;
import java.nio.file.Path;

public class UbiqRunner {
    public static void main(String[] args) throws UbiqException, IOException {
        if (args.length == 1) {
            if (args[0].equals("-i")) {
                new Interpreter().interpret(new String(System.in.readAllBytes()));
            } else {
                new Interpreter().interpret(Path.of(args[0]));
            }
        } else {
            new Repl().replLoop();
        }
    }
}
