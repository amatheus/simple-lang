package dev.ligs.ubiq.interpret;

import dev.ligs.ubiq.sys.Environment;

public interface Evaluatable {
    Object eval(Environment env);
}
